#!/usr/bin/env node

import { tmpdir } from 'node:os';
import { dirname } from 'node:path';
import { createWriteStream, createReadStream, mkdirSync } from 'fs';
import { Command } from 'commander';
import got from 'got';
import { GITLAB_HOST, header } from './env.js';
import { JSONRespCmp, type JSONResp } from './types.js';
import unzipper from 'unzipper';
import { SingleBar } from 'cli-progress';
import { log, err, grey, err_tag, ok_tag, hilight, link } from './colorutil.js';

// main entrypoint
const program = new Command();
program
    .name('glafd')
    .description('glafd - cli tool for downloading gitlab artifacts')
    .version('0.0.1')
    .option('-u --url <url>', 'gitlab base url', 'https://gitlab.com');

// ls
program
    .command('ls')
    .description('list available repositories')
    .action(async () => {
        const url_opts = {
            prefixUrl: program.opts().url ?? GITLAB_HOST ?? 'https://gitlab.com',
            headers: header.headers,
        };
        const projects: JSONResp[] = await got(
            'api/v4/projects?membership=true',
            url_opts,
        ).json();
        log(`found ${hilight(projects.length)} projects:`);
        projects.sort(JSONRespCmp).map((p) => {
            log(` - ${p.name} ${grey(' (' + p.default_branch + ')')}`);
        });
    });

// dl
program
    .command('dl')
    .description('download gitlab artifact')
    .argument('repository name', 'repository name in gitlab')
    .option('-p --path <path>', 'download path')
    .option('-t --tag <tag>', 'tag or branch in git that has an artifact')
    .option(
        '-j --job <job>',
        'gitlab job name where artifact were built',
        'build',
    )
    .action(async (dl, options) => {
        const dl_path = options.path ?? tmpdir();
        let tag: string | undefined;
        let id: number | undefined;
        const url_opts = {
            prefixUrl: program.opts().url ?? GITLAB_HOST ?? 'https://gitlab.com',
            headers: header.headers,
        };
        // Map repository name to id. Gitlab API only supports querying project ids.
        {
            const projects: JSONResp[] = await got(
                'api/v4/projects?membership=true',
                url_opts,
            ).json();
            const project = projects.find((p) => p.name === dl);
            if (!project) {
                err(`${err_tag} no such repository`);
                process.exit(1);
            } else {
                id = project.id;
                tag = options.tag ?? project.default_branch;
            }
        }

        // Actual download code starts here
        const artifact_file = `${dl}${options.tag ? '-' + tag : ''}.zip`;
        const url = `api/v4/projects/${id}/jobs/artifacts/${tag}/download?job=${options.job}`;

        // Progress bar
        const dl_progress = new SingleBar({
            stopOnComplete: true,
            format: `${dl_path}/${artifact_file} [{bar}] {percentage}% | ETA: {eta}s | {value}/{total}`,
        });
        dl_progress.start(100, 0);

        // Stream error handler
        const stream_error_handler = (e: Error) => {
            dl_progress.stop();
            err(`${err_tag} ${e.message}`);
            err(
                `${err_tag} while downloading ${dl_path}/${artifact_file} from ${link(
                    url,
                )}`,
            );
        };

        // Starting of download stream
        const stream = got
            .stream(url, url_opts)
            .on('error', stream_error_handler)
            .on('downloadProgress', ({ percent }) => {
                const percentage = Math.round(percent * 100);
                dl_progress.update(percentage);
            })
            .pipe(createWriteStream(`${dl_path}/${artifact_file}`));

        // Unzip
        stream.on('finish', () => {
            dl_progress.stop();
            log(`extracting ${artifact_file}`);
            let unzipped_entries = 0;
            createReadStream(`${dl_path}/${artifact_file}`).pipe(
                unzipper
                    .Parse()
                    .on('entry', (entry) => {
                        mkdirSync(dirname(entry.path), { recursive: true });
                        if (entry.type === 'File') {
                            entry.pipe(createWriteStream(entry.path));
                        } else {
                            entry.autodrain();
                        }
                        unzipped_entries = unzipped_entries + 1;
                    })
                    .on('finish', () => {
                        log(`${ok_tag} unzipped ${hilight(unzipped_entries)} entries`);
                    }),
            );
        });
    });

program
    .parseAsync(process.argv)
    .then(() => {})
    .catch((e) => {
        err(`${err_tag} ${e}`);
        process.exit(1);
    });
