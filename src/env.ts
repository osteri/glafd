/**
 * @remark All environment variable checks happen here. When this file is imported, it should always validate credentials and fail early if validation fails.
 */
function validate_gitlab_credentials() {
    if (!process.env.GITLAB_TOKEN) {
        console.error('GITLAB_HOST and GITLAB_TOKEN env variables must be set!');
        process.exit(1);
    }
}

validate_gitlab_credentials();

export const GITLAB_HOST = process.env.GITLAB_HOST;
export const GITLAB_TOKEN = process.env.GITLAB_TOKEN;
export const header = {
    headers: {
        'PRIVATE-TOKEN': GITLAB_TOKEN,
    },
};
