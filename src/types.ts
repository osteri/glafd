export interface JSONResp {
  id: number;
  name: string;
  default_branch: string;
}

export function JSONRespCmp(a: JSONResp, b: JSONResp) {
    if (a.name < b.name) return -1;
    else if (b.name < a.name) return 1;
    return 0;
}
