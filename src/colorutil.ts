import chalk from 'chalk';

export const log = console.log;
export const err = console.error;

export const green = chalk.green;
export const red = chalk.red;
export const grey = chalk.grey;

export const ok_tag = `[${chalk.white.bold(green(' OK '))}]`; // [ OK ]
export const err_tag = `[${chalk.white.bold(red(' ERROR '))}]`; // [ ERROR ]

export function hilight(text: string | number) {
    return chalk.bgGrey.whiteBright.bold(` ${text} `);
}

export function link(text: string) {
    return chalk.blue.underline(`${text}`);
}
