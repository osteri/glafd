# Glafd

**G**it**l**ab **a**rti**f**act **d**ownloader – advanced cli utility for downloading Gitlab artifacts

Memory friendly: uses Node streams for downloading/unzipping artifacts.

## Install
```sh
npm i
```
Set `GITLAB_TOKEN` environment variable.

## Usage

List available projects:
```sh
glafd ls
```

Download latest artifact from default branch (usually `main` or `master`):
```sh
glafd dl repo-name
```

Download artifact from non-default url:
```sh
glafd dl repo-name --url https://company.gitlab.com
```

Environment variable `GITLAB_HOST` can also be set for less repetition.

Download tagged artifact:
```sh
glafd dl repo-name --tag 1.0.0
glafd dl repo-name -t 1.0.0
```

Download artifact from non-default build stage. Stages are defined in your `.gitlab-ci`:
```sh
glafd dl repo-name --job myjob
glafd dl repo-name -j myjob
```

More help:
```sh
glafd dl --help
```
