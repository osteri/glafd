# install local dependencies
install:
	npm i

build:
	npm run build

clean:
	rm -rf dist

# global install
install-g: build
	npm install -g .
	@echo
	@echo "*----------------------*"
	@echo "| glafd is now in PATH |"
	@echo "*----------------------*"
	@echo

# global uninstall
uninstall-g:
	npm uninstall -g .
